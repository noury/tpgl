Class {
	#name : #BaselineOfTp1,
	#superclass : #BaselineOf,
	#category : #BaselineOfTp1
}

{ #category : #baselines }
BaselineOfTp1 >> baseline: spec [
	<baseline>

	spec for: #'common' do: [
		spec
			baseline: 'EasyUI' with: [spec repository: 'github://bouraqadi/PharoMisc' ].
			
		spec
			package: #AfficheurLED with: [spec requires: #('EasyUI'). ].
	]
]
