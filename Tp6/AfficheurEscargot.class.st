Class {
	#name : #AfficheurEscargot,
	#superclass : #Object,
	#instVars : [
		'position',
		'estAffiche',
		'imageCourante',
		'imageEscargot',
		'imageCoquille'
	],
	#category : #Tp6
}

{ #category : #affichage }
AfficheurEscargot >> arreterAffichage [
	estAffiche := false.
	imageCourante delete
]

{ #category : #affichage }
AfficheurEscargot >> demarrerAffichage [
	estAffiche := true.
	imageCourante openInWorld

]

{ #category : #'initialize-release' }
AfficheurEscargot >> estAfficheParDefaut [
	^false
]

{ #category : #'initialize-release' }
AfficheurEscargot >> image: nomFichier [
	^ImageMorph withForm: (Form fromFileNamed: nomFichier)
]

{ #category : #affichage }
AfficheurEscargot >> imageCourante: nouvelleImage [
	| doitEtreAffiche |
	doitEtreAffiche := estAffiche.
	doitEtreAffiche ifTrue: [
		self arreterAffichage].
	imageCourante := nouvelleImage.
	imageCourante position: self position.
	doitEtreAffiche ifTrue: [
		self demarrerAffichage]
]

{ #category : #'initialize-release' }
AfficheurEscargot >> initialize [
	super initialize.
	position := self positionParDefaut.
	estAffiche := self estAfficheParDefaut.
	imageEscargot := self image: 'escargot.png'.
	imageCoquille := self image: 'coquille.png'.
	self sort.

]

{ #category : #observation }
AfficheurEscargot >> mettreAJourApresDeplacement: evenementDeplacement [
	self position: evenementDeplacement nouvellePosition
	
]

{ #category : #observation }
AfficheurEscargot >> mettreAJourApresReveil: evenementReveil [
	self sort
]

{ #category : #observation }
AfficheurEscargot >> mettreAJourApresSommeil: evenementSommeil [
	self rentre
]

{ #category : #accessing }
AfficheurEscargot >> position [
	^ position
]

{ #category : #accessing }
AfficheurEscargot >> position: unPoint [
	position := unPoint.
	imageCourante position: unPoint
]

{ #category : #'initialize-release' }
AfficheurEscargot >> positionParDefaut [
	^0@0
]

{ #category : #affichage }
AfficheurEscargot >> rentre [
	self imageCourante: imageCoquille
]

{ #category : #affichage }
AfficheurEscargot >> sort [
	self imageCourante: imageEscargot
]
