Class {
	#name : #AfficheurLigneArrivee,
	#superclass : #Morph,
	#category : #Tp6
}

{ #category : #affichage }
AfficheurLigneArrivee >> arreterAffichage [

	self delete
]

{ #category : #initialization }
AfficheurLigneArrivee >> couleurParDefaut [

	^ Color red
]

{ #category : #affichage }
AfficheurLigneArrivee >> demarrerAffichage [
self openInWorld
]

{ #category : #initialization }
AfficheurLigneArrivee >> hauteurParDefaut [
	^World extent y
]

{ #category : #initialization }
AfficheurLigneArrivee >> initialize [

	super initialize.
	self color: self couleurParDefaut.
	self extent: self largeurParDefaut @ self hauteurParDefaut.
]

{ #category : #initialization }
AfficheurLigneArrivee >> largeurParDefaut [
	^10
]

{ #category : #accessing }
AfficheurLigneArrivee >> xArrivee: unEntier [
	self left: unEntier - self width  
]
