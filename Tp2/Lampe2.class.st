Class {
	#name : #Lampe2,
	#superclass : #Object,
	#instVars : [
		'estAllumee',
		'couleur',
		'afficheur'
	],
	#category : #Tp2
}

{ #category : #examples }
Lampe2 class >> affichageGraphique [
	<example>
	| afficheur |
	afficheur := AfficheurLEDRonde new.
	self demoLampeAvecAfficheur: afficheur.
]

{ #category : #examples }
Lampe2 class >> affichageTextuel [
	<example>
	| fentereTranscript |
	fentereTranscript := Transcript
		clear;
		open.
	self demoLampeAvecAfficheur: AfficheurTextuelLampe new.
	fentereTranscript close.
	
]

{ #category : #examples }
Lampe2 class >> demoLampeAvecAfficheur: unAfficheur [
	| lampe |
	lampe := self new.
	lampe afficheur: unAfficheur.
	self halt.	"Point d'arrêt pour faire du pas à pas et voir l'exemple évoluer" 
	lampe allumer.
	lampe eteindre.
	lampe couleur: Color red.
	lampe allumer.
	lampe couleur: Color blue.
	lampe arreterAffichage.
	lampe couleur: Color green.
	lampe eteindre.
	lampe demarrerAffichage.
	lampe allumer.
	lampe arreterAffichage.

]

{ #category : #accessing }
Lampe2 >> afficheur [
	^ afficheur
]

{ #category : #accessing }
Lampe2 >> afficheur: anObject [
	afficheur := anObject.
	self mettreAJourAfficheur
]

{ #category : #etat }
Lampe2 >> allumer [
	self estAllumee: true
]

{ #category : #affichage }
Lampe2 >> arreterAffichage [
	self afficheur ifNil: [ ^self ].
	self afficheur arreterAffichage
]

{ #category : #apparence }
Lampe2 >> couleur [
	self estAllumee ifFalse: [ ^self couleurLampeEteinte ].
	^ couleur
]

{ #category : #apparence }
Lampe2 >> couleur: uneCouleur [
	couleur := uneCouleur.
	self mettreAJourAfficheur
]

{ #category : #apparence }
Lampe2 >> couleurLampeEteinte [
	^Color black
]

{ #category : #initialisation }
Lampe2 >> couleurParDefaut [
	^Color yellow
]

{ #category : #affichage }
Lampe2 >> demarrerAffichage [
	self afficheur ifNil: [^self].
	self afficheur demarrerAffichage
]

{ #category : #etat }
Lampe2 >> estAllumee [
	^ estAllumee
]

{ #category : #etat }
Lampe2 >> estAllumee: unBooleen [
	estAllumee := unBooleen.
	self mettreAJourAfficheur 
]

{ #category : #initialisation }
Lampe2 >> estAllumeeParDefaut [
	^false
]

{ #category : #etat }
Lampe2 >> eteindre [
	self estAllumee: false
]

{ #category : #initialisation }
Lampe2 >> initialize [
	super initialize.
	self estAllumee: self estAllumeeParDefaut.
	self couleur: self couleurParDefaut 
]

{ #category : #apparence }
Lampe2 >> mettreAJourAfficheur [
	self afficheur ifNil: [ ^self ].
	self afficheur miseAJourAvec: self
]
