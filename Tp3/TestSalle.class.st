Class {
	#name : #TestSalle,
	#superclass : #TestCase,
	#instVars : [
		'salle'
	],
	#category : #Tp3
}

{ #category : #testing }
TestSalle >> setUp [
	super setUp.
	salle := Salle new
]

{ #category : #testing }
TestSalle >> test100PlacesLibreJusteApresLaCreation [
	self assert: salle nombrePlacesLibres equals: 100
]

{ #category : #testing }
TestSalle >> test120PlacesLibreDansUneSalleVideDe120Sieges [
	salle nombreSieges: 120.
	self assert: salle nombrePlacesLibres equals: 120
]

{ #category : #testing }
TestSalle >> test250PlacesLibreDansUneSalleVideDe250Sieges [
	salle nombreSieges: 250.
	self assert: salle nombrePlacesLibres equals: 250
]

{ #category : #testing }
TestSalle >> testChangementDuTitreDeFilmEnHarryPotter [
	salle titreFilm: 'Harry Potter'.
	self assert: salle titreFilm equals: 'Harry Potter'
]

{ #category : #testing }
TestSalle >> testChangementDuTitreDeFilmEnStarWars [
	salle titreFilm: 'Star Wars'.
	self assert: salle titreFilm equals: 'Star Wars'
]

{ #category : #testing }
TestSalle >> testChiffreAffaire110VenteDe10PlacesTarifNormalEt5TarifReduit [
	salle vendrePlacesTarifNormal: 10.
	salle vendrePlacesTarifReduit: 5.
	self assert: salle chiffreAffaires equals: 110
]

{ #category : #testing }
TestSalle >> testChiffreAffaire330Film3DVenteDe25PlacesTarifNormalEt10TarifReduit [
	salle filmEn3D: true.
	salle vendrePlacesTarifNormal: 25.
	salle vendrePlacesTarifReduit: 10.
	self assert: salle chiffreAffaires equals: 330
]

{ #category : #testing }
TestSalle >> testPasDeTitreDeFilmJusteApresLaCreation [
	self assert: salle titreFilm equals: '---'
]

{ #category : #testing }
TestSalle >> testPrixPlaceReduitNormalFilm3dEst8 [
	salle filmEn3D: true.
	self assert: salle prixPlaceTarifReduit equals: 8
]

{ #category : #testing }
TestSalle >> testPrixPlaceTarifNormalEst8ApresCorrectionFilm2dAuLieuDe2d [
	salle filmEn3D: true.
	salle filmEn3D: false.
	self assert: salle prixPlaceTarifNormal equals: 8
]

{ #category : #testing }
TestSalle >> testPrixPlaceTarifNormalFilm3dEst10 [
	salle filmEn3D: true.
	self assert: salle prixPlaceTarifNormal equals: 10
]

{ #category : #testing }
TestSalle >> testPrixPlaceTarifNormalParDefautEst8 [
	self assert: salle prixPlaceTarifNormal equals: 8
]

{ #category : #testing }
TestSalle >> testPrixPlaceTarifReduitEst6ApresCorrectionFilm2dAuLieuDe2d [
	salle filmEn3D: true.
	salle filmEn3D: false.
	self assert: salle prixPlaceTarifReduit equals: 6
]

{ #category : #testing }
TestSalle >> testPrixPlaceTarifReduitParDefautEst6 [
	self assert: salle prixPlaceTarifReduit equals: 6
]

{ #category : #testing }
TestSalle >> testVenteDe30PlacesTarifNormalReste70PlacesLibres [
	salle vendrePlacesTarifNormal: 30.
	self assert: salle nombrePlacesLibres equals: 70
]

{ #category : #testing }
TestSalle >> testVenteDe30PlacesTarifReduitReste70PlacesLibres [
	salle vendrePlacesTarifReduit: 30.
	self assert: salle nombrePlacesLibres equals: 70
]

{ #category : #testing }
TestSalle >> testVenteSuccessiveDe10Et25PlacesTarifNormalReste65PlacesLibres [
	salle vendrePlacesTarifNormal: 10.
	salle vendrePlacesTarifNormal: 25.
	self assert: salle nombrePlacesLibres equals: 65
]

{ #category : #testing }
TestSalle >> testVenteSuccessiveDe10Et25PlacesTarifReduitReste65PlacesLibres [
	salle vendrePlacesTarifReduit: 10.
	salle vendrePlacesTarifReduit: 25.
	self assert: salle nombrePlacesLibres equals: 65
]

{ #category : #testing }
TestSalle >> testVenteSuccessiveDe42PlacesTarifNormalEt50TarifReduitReste8PlacesLibres [
	salle vendrePlacesTarifNormal: 42.
	salle vendrePlacesTarifReduit: 50.
	self assert: salle nombrePlacesLibres equals: 8
]
