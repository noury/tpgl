Class {
	#name : #Cinema,
	#superclass : #Object,
	#instVars : [
		'salles'
	],
	#category : #Tp3
}

{ #category : #vente }
Cinema >> chiffreAffaires [
	^self salles inject: 0 into: [:total :salle| total + salle chiffreAffaires ]
]

{ #category : #'initialize - release' }
Cinema >> initialize [
	super initialize.
	self salles: self sallesParDefaut
]

{ #category : #vente }
Cinema >> remiseAZeroDesVentes [
	self salles do: [ :uneSalle | uneSalle remiseAZeroDesVentes ]
]

{ #category : #accessing }
Cinema >> salleNumero: entier [
	^self salles at: entier
]

{ #category : #accessing }
Cinema >> salles [
	^ salles
]

{ #category : #accessing }
Cinema >> salles: anObject [
	salles := anObject
]

{ #category : #'initialize - release' }
Cinema >> sallesParDefaut [
	| sallesParDefaut |
	sallesParDefaut := OrderedCollection new.
	3 timesRepeat: [
		sallesParDefaut add: Salle new
	].
	(sallesParDefaut at: 3) nombreSieges: 400.
	^sallesParDefaut
	
]
