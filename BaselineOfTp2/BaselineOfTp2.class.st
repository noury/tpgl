Class {
	#name : #BaselineOfTp2,
	#superclass : #BaselineOf,
	#category : #BaselineOfTp2
}

{ #category : #baselines }
BaselineOfTp2 >> baseline: spec [
	<baseline>

	spec for: #'common' do: [
		spec
			baseline: 'EasyUI' with: [spec repository: 'github://bouraqadi/PharoMisc' ].
			
		spec 
			package: #AfficheurLED with: [spec requires: #('EasyUI')];
			package: #Tp2 with: [spec requires: #(AfficheurLED)]. 
		].

]
