Class {
	#name : #FenetreDessin,
	#superclass : #EzApp,
	#category : #Tp4
}

{ #category : #'window handling' }
FenetreDessin >> defaultTitle [
	^'Dessin'
]

{ #category : #initialization }
FenetreDessin >> initialize [
	super initialize.
	self openWindow.
]

{ #category : #dessiner }
FenetreDessin >> ligneDe: origine a: extremite epaisseur: epaisseur couleur: couleur [
	| ligne |
	ligne := EzPolyLine from: origine to: extremite.
	ligne width: epaisseur.
	ligne color: couleur.
	drawingBoard add: ligne.
]
