Class {
	#name : #BaselineOfTp6,
	#superclass : #BaselineOf,
	#category : #BaselineOfTp6
}

{ #category : #baselines }
BaselineOfTp6 >> baseline: spec [

	<baseline>
	spec for: #common do: [

		spec package: #Tp6 ]
]
