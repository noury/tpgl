Class {
	#name : #SimulationBalleSimple,
	#superclass : #Object,
	#instVars : [
		'balle',
		'courbe',
		'afficheurBalle',
		'thread'
	],
	#category : #Tp5
}

{ #category : #accessing }
SimulationBalleSimple >> afficheurBalle [
	^ afficheurBalle
]

{ #category : #fonctionnement }
SimulationBalleSimple >> arreter [
	thread stop
]

{ #category : #accessing }
SimulationBalleSimple >> balle [
	^ balle
]

{ #category : #initialization }
SimulationBalleSimple >> classeBalle [
	^BalleSimple
]

{ #category : #accessing }
SimulationBalleSimple >> courbe [
	^ courbe
]

{ #category : #fonctionnement }
SimulationBalleSimple >> demarrer [
	thread stop.
	self reinitialiserSimulation.
	thread restart
]

{ #category : #fonctionnement }
SimulationBalleSimple >> fermerLesFenetres [
	{afficheurBalle. courbe} do: #closeWindow.
	

]

{ #category : #initialization }
SimulationBalleSimple >> initialize [
	super initialize.
	balle := self classeBalle new.
	afficheurBalle := BallDrawing new.
	afficheurBalle title: afficheurBalle title, ' - ', self className. 
	courbe := CurveDrawing new.
	courbe title: courbe title, ' - ', self className. 
	self lierSujetEtObservateurs.
	thread := TkThread 
		repeat: [self unPasDeSimulation] 
		while: [self poursuiteSimulationAutorisee]
		every: 100 milliSeconds.
	self reinitialiserSimulation.

]

{ #category : #initialization }
SimulationBalleSimple >> positionInitialeBalle [
	^0@500
]

{ #category : #fonctionnement }
SimulationBalleSimple >> poursuiteSimulationAutorisee [
	^afficheurBalle boundingBox containsPoint: balle position
]

{ #category : #initialization }
SimulationBalleSimple >> reinitialiserSimulation [
	self balle position: self positionInitialeBalle.
	self balle vitesse: self vitesseInitialeBalle.
	courbe clear.


]

{ #category : #fonctionnement }
SimulationBalleSimple >> unPasDeSimulation [
	balle unPasDeSimulation
]

{ #category : #initialization }
SimulationBalleSimple >> vitesseInitialeBalle [
	^9 @ -30
]
