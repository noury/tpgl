Class {
	#name : #CurveDrawing,
	#superclass : #NaturalReferentialDrawing,
	#category : #Tp5
}

{ #category : #drawing }
CurveDrawing >> addLineFrom: point1 to: point2 width: lineWidth color: lineColor [
	| line |
	line := EzPolyLine 
		from: (self mathPointFromGraphicsPoint: point1) 
		to: (self mathPointFromGraphicsPoint: point2).
	line width: lineWidth.
	line color: lineColor.
	self drawingBoard add: line.
]

{ #category : #drawing }
CurveDrawing >> clear [
	self drawingBoard removeAll
]

{ #category : #'window handling' }
CurveDrawing >> defaultTitle [
	^'Curve Drawing'
]

{ #category : #drawing }
CurveDrawing >> isCleared [
	^self drawingBoard drawings isEmpty
]
