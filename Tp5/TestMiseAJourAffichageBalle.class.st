Class {
	#name : #TestMiseAJourAffichageBalle,
	#superclass : #TestCase,
	#instVars : [
		'balle',
		'afficheur'
	],
	#category : #Tp5
}

{ #category : #testing }
TestMiseAJourAffichageBalle >> setUp [
	super setUp.
	balle := BalleSimple new.
	afficheur := BallDrawing new.
	self lierSujetEtObservateur
]

{ #category : #testing }
TestMiseAJourAffichageBalle >> tearDown [
	super tearDown.
	afficheur closeWindow
]

{ #category : #testing }
TestMiseAJourAffichageBalle >> testPositionAfficheurMiseAJourAvecCelleDeLaBalle [
	(-100 to: 100 by: 25) do: [ :x|
		(-100 to: 100 by: 25) do: [:y |
		balle position: x@y.
		self assert: afficheur ballViewPosition equals: balle position.
	]]
]
