"
Converts math points to graphics points.
Math points are ones which referential is the bottom-left corner of the drawingBoard.
Graphics points are ones with the default computer graphics referential, i.e. top-left corner.
"
Class {
	#name : #NaturalReferentialDrawing,
	#superclass : #EzApp,
	#category : #Tp5
}

{ #category : #accessing }
NaturalReferentialDrawing >> graphicsPointFromMathPoint: mathPoint [
	| graphicsY |
	graphicsY := self drawingBoard height - mathPoint y.
	^ mathPoint x @ graphicsY
]

{ #category : #initialization }
NaturalReferentialDrawing >> initialize [
	super initialize.
	self openWindow.
]

{ #category : #accessing }
NaturalReferentialDrawing >> mathPointFromGraphicsPoint: graphicsPoint [
	| mathY |
	mathY := self drawingBoard height - graphicsPoint y.
	^ graphicsPoint x @ mathY
]
