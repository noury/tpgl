Class {
	#name : #TestSimulationBalleSimple,
	#superclass : #TestCase,
	#instVars : [
		'sim',
		'balle',
		'courbe',
		'afficheurBalle'
	],
	#category : #Tp5
}

{ #category : #testing }
TestSimulationBalleSimple >> classeSimulation [
	^SimulationBalleSimple
]

{ #category : #testing }
TestSimulationBalleSimple >> setUp [
	super setUp.
	sim := self classeSimulation new.
	balle := sim balle.
	courbe := sim courbe.
	afficheurBalle := sim afficheurBalle.
]

{ #category : #testing }
TestSimulationBalleSimple >> tearDown [
	super tearDown.
	sim arreter.
	courbe closeWindow.
	afficheurBalle closeWindow.
]

{ #category : #testing }
TestSimulationBalleSimple >> testEtatInitial [
	self assert: balle position equals: sim positionInitialeBalle.
	self assert: balle vitesse equals: sim vitesseInitialeBalle.
	self assert: courbe isCleared.
	self assert: afficheurBalle ballViewPosition equals: sim positionInitialeBalle.
	self assert: sim poursuiteSimulationAutorisee. 
]

{ #category : #testing }
TestSimulationBalleSimple >> testUnPasDeSimulation [
	sim unPasDeSimulation.
	self deny: balle position equals: sim positionInitialeBalle.
	self deny: courbe isCleared.
	self assert: afficheurBalle ballViewPosition equals: balle position.
	self assert: sim poursuiteSimulationAutorisee. 
]
