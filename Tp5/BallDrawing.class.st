Class {
	#name : #BallDrawing,
	#superclass : #NaturalReferentialDrawing,
	#instVars : [
		'ballView'
	],
	#category : #Tp5
}

{ #category : #accessing }
BallDrawing >> ballView [
	ballView ifNil: [ self ballViewPosition: 0@0 ].
	^ballView
]

{ #category : #accessing }
BallDrawing >> ballViewCenterOffset [
	^ self ballViewRadius negated @ self ballViewRadius
]

{ #category : #accessing }
BallDrawing >> ballViewPosition [
	^self mathPointFromGraphicsPoint: self ballView center
]

{ #category : #accessing }
BallDrawing >> ballViewPosition: mathPoint [
	| graphicsPoint |
	graphicsPoint := self graphicsPointFromMathPoint: mathPoint.
	ballView ifNil: [ ^ self initBallViewAt: graphicsPoint ].
	ballView center: graphicsPoint 
]

{ #category : #accessing }
BallDrawing >> ballViewRadius [
	^50
]

{ #category : #accessing }
BallDrawing >> boundingBox [
	^0@0 extent: (drawingBoard width @ drawingBoard height) - self ballViewRadius
]

{ #category : #'window handling' }
BallDrawing >> defaultTitle [
	^'Ball Drawing'
]

{ #category : #accessing }
BallDrawing >> graphicsPointFromMathPoint: mathPoint [
	^ (super graphicsPointFromMathPoint: mathPoint) - self ballViewCenterOffset
]

{ #category : #initialization }
BallDrawing >> initBallViewAt: aPosition [
	ballView := EzOval circleCenter: aPosition radius: self ballViewRadius.
	drawingBoard add: ballView
]

{ #category : #accessing }
BallDrawing >> mathPointFromGraphicsPoint: graphicsPoint [
	^super mathPointFromGraphicsPoint: graphicsPoint + self ballViewCenterOffset
]
