Class {
	#name : #BaselineOfTp5,
	#superclass : #BaselineOf,
	#category : #BaselineOfTp5
}

{ #category : #baselines }
BaselineOfTp5 >> baseline: spec [
	<baseline>

	spec for: #'common' do: [
		spec
			baseline: 'EasyUI' with: [spec repository: 'github://bouraqadi/PharoMisc'];
			baseline: 'Tasks' with: [spec repository: 'github://bouraqadi/PharoMisc'].	

		spec 
			package: #Tp5 with: [spec requires: #(EasyUI Tasks)]. 
	]
]
