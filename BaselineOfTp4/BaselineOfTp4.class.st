Class {
	#name : #BaselineOfTp4,
	#superclass : #BaselineOf,
	#category : #BaselineOfTp4
}

{ #category : #baselines }
BaselineOfTp4 >> baseline: spec [
	<baseline>

	spec for: #'common' do: [
		spec
			baseline: 'EasyUI' with: [spec repository: 'github://bouraqadi/PharoMisc' ].	

		spec 
			package: #Tp4 with: [spec requires: #(EasyUI)]. 
	]
]
