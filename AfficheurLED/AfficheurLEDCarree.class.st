Class {
	#name : #AfficheurLEDCarree,
	#superclass : #EzBox,
	#traits : 'TAfficheurLED',
	#classTraits : 'TAfficheurLED classTrait',
	#category : #AfficheurLED
}

{ #category : #accessing }
AfficheurLEDCarree >> longueurCote: uneLongueur [
	self width: uneLongueur height: uneLongueur
]
