Class {
	#name : #EzAfficheurLEDApp,
	#superclass : #EzApp,
	#category : #AfficheurLED
}

{ #category : #initialization }
EzAfficheurLEDApp >> defaultTitle [
	^'Afficheur LED'
]

{ #category : #initialization }
EzAfficheurLEDApp >> drawingBoardExtent [
	^ 1000 @ 800
]
