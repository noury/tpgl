Trait {
	#name : #TAfficheurLED,
	#classInstVars : [
		'app'
	],
	#category : #AfficheurLED
}

{ #category : #accessing }
TAfficheurLED classSide >> app [
	^app ifNil: [ app := EzAfficheurLEDApp new ]
]

{ #category : #accessing }
TAfficheurLED classSide >> drawingBoard [
	^self app drawingBoard
]

{ #category : #affichage }
TAfficheurLED classSide >> fermerFenetre [
	self window ifNil: [ ^self ].
	self window delete.
]

{ #category : #accessing }
TAfficheurLED classSide >> length [
	^50
]

{ #category : #accessing }
TAfficheurLED classSide >> newCenter [
	self drawingBoard drawings ifEmpty: [ ^self length @ self length ].
	^self drawingBoard drawings last center + 10.
	
]

{ #category : #affichage }
TAfficheurLED classSide >> openWindow [
	self window ifNotNil: [self window isDisplayed ifTrue: [ ^self ]].
	self app openWindow
]

{ #category : #affichage }
TAfficheurLED classSide >> openWindowWith: aDrawing [
	self drawingBoard add: aDrawing.
	self openWindow. 
]

{ #category : #affichage }
TAfficheurLED classSide >> resetApp [
	app ifNil: [ ^self ].
	self fermerFenetre.
	app := nil.
]

{ #category : #affichage }
TAfficheurLED classSide >> toutSupprimer [
	self drawingBoard removeAll
]

{ #category : #accessing }
TAfficheurLED classSide >> window [
	self app windowPresenter ifNil: [ ^nil ].
	^self app window
]

{ #category : #affichage }
TAfficheurLED >> arreterAffichage [
	self class drawingBoard remove: self
]

{ #category : #affichage }
TAfficheurLED >> demarrerAffichage [
	self show.
	self class openWindowWith: self.

]

{ #category : #affichage }
TAfficheurLED >> fermerFenetre [
	self class fermerFenetre	
]

{ #category : #initialization }
TAfficheurLED >> initialize [
	super initialize.
	self width: self class length height: self class length.
	self center: self class newCenter.
	self fillColor: Color yellow.
	self demarrerAffichage.
]

{ #category : #affichage }
TAfficheurLED >> miseAJourAvec: uneLampe [
	self fillColor: uneLampe couleur
]

{ #category : #accessing }
TAfficheurLED >> position [
	^self origin
]

{ #category : #accessing }
TAfficheurLED >> position: aPoint [
	self origin: aPoint
]
