Class {
	#name : #AfficheurLEDRonde,
	#superclass : #EzOval,
	#traits : 'TAfficheurLED',
	#classTraits : 'TAfficheurLED classTrait',
	#category : #AfficheurLED
}

{ #category : #accessing }
AfficheurLEDRonde >> diametre [
	^self width
]
